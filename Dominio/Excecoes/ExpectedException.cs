﻿using System;

namespace Dominio.Excecoes
{
    public class ExpectedException : Exception
    {
        public ExpectedException()
        {
        }

        public ExpectedException(string message)
            : base(message)
        {
        }

        public ExpectedException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
