﻿namespace Dominio.Entidades
{
    public class Produto
    {
        public int? Id { get; set; }

        public string Nome { get; set; }

        public double Valor { get; set; }

        public string Cor { get; set; }

        public double Peso { get; set; }

        public string Observacao { get; set; }

        public Produto()
        {

        }
    }
}
