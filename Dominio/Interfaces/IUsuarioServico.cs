﻿using Aplicacao.Interfaces;
using Dominio.Entidades;

namespace Dominio.Interfaces
{
    public interface IUsuarioServico : IServicos
    {
        void LogarUsuario(Usuario usuario);
    }
}
