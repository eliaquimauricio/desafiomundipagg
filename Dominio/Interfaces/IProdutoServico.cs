﻿using Aplicacao.Interfaces;
using Dominio.Entidades;
using System.Linq;

namespace Dominio.Interfaces
{
    public interface IProdutoServico : IServicos
    {
        int InserirProduto(Produto produto);
        
        void EditarProduto(Produto produto);

        void ExcluirProduto(int id);

        Produto ProverDetalheProduto(int id);

        IQueryable<Produto> ProverDetalheTodosProdutos();

    }
}
