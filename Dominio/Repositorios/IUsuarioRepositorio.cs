﻿using Dominio.Entidades;

namespace Dominio.Repositorios
{
    public interface IUsuarioRepositorio
    {
        Usuario ConsultarUsuario(string id, string chaveAcesso);
    }
}
