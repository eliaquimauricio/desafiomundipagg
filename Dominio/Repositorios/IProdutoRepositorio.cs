﻿using Dominio.Entidades;
using System.Linq;

namespace Dominio.Repositorios
{
    public interface IProdutoRepositorio
    {
        int AdicionarProduto(Produto produto);

        bool ExisteProduto(int? id);

        void EditarProduto(Produto produto);

        void ExcluirProduto(int id);

        Produto ConsultarProduto(int id);

        IQueryable<Produto> ConsultarTodosProdutos();
    }
}
