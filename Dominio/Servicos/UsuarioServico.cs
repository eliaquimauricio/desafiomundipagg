﻿using Dominio.Entidades;
using Dominio.Excecoes;
using Dominio.Interfaces;
using Dominio.Repositorios;
using System;

namespace Dominio.Servicos
{
    public class UsuarioServico : IUsuarioServico
    {
        private readonly IUsuarioRepositorio _usuarioRepositorio;

        public UsuarioServico(IUsuarioRepositorio usuarioRepositorio)
        {
            _usuarioRepositorio = usuarioRepositorio;
        }

        public void LogarUsuario(Usuario usuario)
        {
            if (usuario == null)
                throw new ExpectedException("Paramêtro usuário está nulo.");

            if (usuario.Id == string.Empty)
                throw new ExpectedException("O Id do usuário não foi informado corretamente.");

            if (usuario.ChaveAcesso == string.Empty)
                throw new ExpectedException("A chave de acesso do usuário não foi informado corretamente.");
            
            var resultado = _usuarioRepositorio.ConsultarUsuario(usuario.Id, usuario.ChaveAcesso);

            if (resultado == null)
                throw new ExpectedException("Usuário ou chave de acesso incorretas.");
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
