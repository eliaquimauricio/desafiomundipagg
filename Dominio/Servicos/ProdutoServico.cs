﻿using Aplicacao.Interfaces;
using Dominio.Entidades;
using Dominio.Excecoes;
using Dominio.Interfaces;
using Dominio.Repositorios;
using System;
using System.Linq;

namespace Dominio.Servicos
{
    public class ProdutoServico : IProdutoServico
    {
        private readonly IProdutoRepositorio _produtoRepositorio;

        public ProdutoServico(IProdutoRepositorio produtoRepositorio)
        {
            _produtoRepositorio = produtoRepositorio;
        }

        private void ValidaProduto(Produto produto)
        {
            if (produto == null)
                throw new ExpectedException("O paramêtro de entrada se encontra nulo.");

            if (produto.Nome.Trim().Length == 0)
                throw new ExpectedException("O nome precisa ser uma string não vazia.");

            if (produto.Valor <= 0)
                throw new ExpectedException("O valor do produto precisa de um float válido diferente de zero.");

            if (produto.Peso <= 0)
                throw new ExpectedException("O peso do produto precisa de um float válido diferente de zero.");
        }

        public int InserirProduto(Produto produto)
        {
            ValidaProduto(produto);
            
            return _produtoRepositorio.AdicionarProduto(produto);
        }

        public void EditarProduto(Produto produto)
        {
            ValidaProduto(produto);

            if ((produto.Id == null) || (produto.Id <= 0))
                throw new ExpectedException("O código precisa ser informado e ser um valor inteiro maior que zero.");
            
                if (!_produtoRepositorio.ExisteProduto(produto.Id))
                    throw new ExpectedException("O código de produto informado não existe.");

            _produtoRepositorio.EditarProduto(produto);
        }

        public void ExcluirProduto(int id)
        {
            if (id <= 0)
                throw new ExpectedException("O código precisa ser informado e ser um valor inteiro maior que zero.");
            
            if (!_produtoRepositorio.ExisteProduto(id))
                throw new ExpectedException("O código de produto informado não existe.");

            _produtoRepositorio.ExcluirProduto(id);
        }

        public Produto ProverDetalheProduto(int id)
        {
            if (id <= 0)
                throw new ExpectedException("O código precisa ser informado e ser um valor inteiro maior que zero.");
            
            if (!_produtoRepositorio.ExisteProduto(id))
                throw new ExpectedException("O código de produto informado não existe.");

            return _produtoRepositorio.ConsultarProduto(id);
        }

        public IQueryable<Produto> ProverDetalheTodosProdutos()
        {
            IQueryable<Produto> produtos = _produtoRepositorio.ConsultarTodosProdutos();

            if (produtos.Count() == 0)
                throw new ExpectedException("Nenhum produto cadastrado.");
         
            return produtos;
        }
                
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
