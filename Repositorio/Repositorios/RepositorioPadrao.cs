﻿using Repositorio.Contexto;
using System;

namespace Repositorio.Repositorios
{
    public class RepositorioPadrao : IDisposable
    {
        protected ContextoPrincipal ContextoPrincipal;

        public RepositorioPadrao()
        {
            ContextoPrincipal = ContextoPrincipal.Instancia;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
