﻿using Dominio.Entidades;
using Dominio.Repositorios;
using Repositorio.Contexto;
using System.Linq;

namespace Repositorio.Repositorios
{
    public class UsuarioRepositorio : RepositorioPadrao, IUsuarioRepositorio
    {
        public UsuarioRepositorio() : base()
        {
            
        }

        public Usuario ConsultarUsuario(string id, string chaveAcesso)
        {
            return (from p in ContextoPrincipal.Usuarios
                    where (p.Id.Equals(id) && p.ChaveAcesso.Equals(chaveAcesso))
                    select p).FirstOrDefault();
        }
    }
}
