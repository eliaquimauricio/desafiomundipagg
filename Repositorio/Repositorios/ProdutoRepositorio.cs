﻿using Dominio.Entidades;
using Dominio.Repositorios;
using Microsoft.EntityFrameworkCore;
using Repositorio.Contexto;
using System.Linq;

namespace Repositorio.Repositorios
{
    public class ProdutoRepositorio : RepositorioPadrao, IProdutoRepositorio
    {        
        public ProdutoRepositorio() : base()
        {
            
        }

        public int AdicionarProduto(Produto produto)
        {
            ContextoPrincipal.Produtos.Add(produto);
            ContextoPrincipal.SaveChanges();
            return produto.Id ?? 0;
        }        

        public bool ExisteProduto(int? id)
        {
            return ContextoPrincipal.Produtos.Where(c => c.Id == id).FirstOrDefault() != null;            
        }

        public void EditarProduto(Produto produto)
        {
            var original = ContextoPrincipal.Produtos.Find(produto.Id);            
            ContextoPrincipal.Entry(original).CurrentValues.SetValues(produto);
            ContextoPrincipal.SaveChanges();
        }

        public void ExcluirProduto(int id)
        {
            var original = ContextoPrincipal.Produtos.Where(c => c.Id.Equals(id)).First();
            ContextoPrincipal.Entry(original).State = EntityState.Deleted;
            ContextoPrincipal.SaveChanges();
        }

        public Produto ConsultarProduto(int id)
        {
            return (from p in ContextoPrincipal.Produtos
                    where (p.Id.Equals(id))
                    select p).First();
        }

        public IQueryable<Produto> ConsultarTodosProdutos()
        {      
            return (from p in ContextoPrincipal.Produtos
                                     select p);
        }
    }
}
