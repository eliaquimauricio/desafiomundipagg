﻿using Dominio.Entidades;
using Microsoft.EntityFrameworkCore;

namespace Repositorio.Contexto
{
    public class ContextoPrincipal : DbContext
    {
        private static ContextoPrincipal _instancia;

        public static ContextoPrincipal Instancia { get { return _instancia ?? new ContextoPrincipal(); } }

        private ContextoPrincipal()
        {

        }

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Produto> Produtos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder opcoes)
        {
            opcoes.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=LojaDB;Trusted_Connection=true;"); //TODO: Adicionar no arquivo de configuração.
        }
    }
}
