﻿using Aplicacao;
using Aplicacao.MapeamentoAutomatico;
using Aplicacao.Modelos;
using Dominio.Servicos;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Repositorio.Repositorios;
using System;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;            
        }

        public IConfiguration Configuration { get; }
                
        public void ConfigureServices(IServiceCollection services)
        {            
            services.AddSingleton(new UsuarioAppServico(new UsuarioServico(new UsuarioRepositorio())));
            services.AddSingleton(new ProdutoAppServico(new ProdutoServico(new ProdutoRepositorio())));

            ConfiguracoesAssinatura configuracoesAssinatura = new ConfiguracoesAssinatura();
            services.AddSingleton(configuracoesAssinatura);
            
            TokenConfiguracoesModelo tokenConfiguracoes = new TokenConfiguracoesModelo();

            new ConfigureFromConfigurationOptions<TokenConfiguracoesModelo>(Configuration.GetSection("TokenConfigurations")).Configure(tokenConfiguracoes);
            services.AddSingleton(tokenConfiguracoes);

            services.AddAuthentication(authOptions =>
            {
                authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                authOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(bearerOptions =>
            {
                var paramsValidation = bearerOptions.TokenValidationParameters;
                paramsValidation.IssuerSigningKey = configuracoesAssinatura.Chave;
                paramsValidation.ValidAudience = tokenConfiguracoes.Audiencia;
                paramsValidation.ValidIssuer = tokenConfiguracoes.Emissor;
                paramsValidation.ValidateIssuerSigningKey = true;
                paramsValidation.ValidateLifetime = true;
                paramsValidation.ClockSkew = TimeSpan.FromMinutes(10); // Tempo de tolêrancia.
            });
                        
            services.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser().Build());
            });

            services.AddMvc();
        }
                
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            MapeamentoAutomaticoConfig.RegistrarMapeamento();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseMvc();
        }            
    }
}
