﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebApi
{
    public class Paginacao<T> : List<T>
    {
        public int IndicePagina { get; private set; }
        public int TotalPaginas { get; private set; }
                
        public Paginacao(List<T> items, int count, int indicePagina, int tamanhoPagina)
        {
            IndicePagina = indicePagina;
            TotalPaginas = (int)Math.Ceiling(count / (double)tamanhoPagina);
            AddRange(items);
        }
    
        public static async Task<Paginacao<T>> CreateAsync(IQueryable<T> source, int indicePagina, int tamanhoPagina)
        {
            int quantidade = await source.CountAsync();
            List<T> items = await source.Skip((indicePagina - 1) * tamanhoPagina).Take(tamanhoPagina).ToListAsync();
            return new Paginacao<T>(items, quantidade, indicePagina, tamanhoPagina);
        }
    }
}

