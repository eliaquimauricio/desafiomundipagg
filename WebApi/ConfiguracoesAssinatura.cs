﻿using System.Security.Cryptography;
using Microsoft.IdentityModel.Tokens;

namespace WebApi
{
    public class ConfiguracoesAssinatura 
    {
        public SecurityKey Chave { get; }
        public SigningCredentials SigningCredentials { get; }

        public ConfiguracoesAssinatura()
        {
            using (var provider = new RSACryptoServiceProvider(2048))
            {
                Chave = new RsaSecurityKey(provider.ExportParameters(true));
            }

            SigningCredentials = new SigningCredentials(Chave, SecurityAlgorithms.RsaSha256Signature);
        }
    }
}