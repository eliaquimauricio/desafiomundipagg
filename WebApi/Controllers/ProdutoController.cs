﻿using Aplicacao;
using Aplicacao.Modelos;
using Aplicacao.Utilidades;
using Dominio.Entidades;
using Dominio.Excecoes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ProdutoController : Controller
    {
        [AllowAnonymous]
        [HttpGet]
        public string Iniciar()
        {
            return "A API foi iniciada corretamente.";
        }

        [AllowAnonymous]
        [HttpPost("gerarToken")]
        public IActionResult GerarToken([FromBody]UsuarioModelo usuario, [FromServices]UsuarioAppServico servicoUsuario,
                                        [FromServices]ConfiguracoesAssinatura configuracoesAssinatura,
                                        [FromServices]TokenConfiguracoesModelo tokenConfiguracoes)
        {
            try
            {
                servicoUsuario.LogarUsuario(usuario);

                string token = ProverToken(usuario, tokenConfiguracoes, configuracoesAssinatura);

                return Ok(new { accessToken = token });
            }
            catch (ExpectedException ex)
            {
                return Utilidades.BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return Utilidades.InternalServerError(ex.Message);
            }
        }
            
        [Authorize("Bearer")]
        [HttpPost]        
        [Route("inserirProduto")]
        public IActionResult InserirProduto([FromBody]ProdutoModelo produto, [FromServices]ProdutoAppServico produtoServico)
        {
            try
            {
                int codigoProduto = produtoServico.InserirProduto(produto);

                return Ok(new {
                    Mensagem = "Produto inserido com sucesso.",
                    CodigoProduto = codigoProduto
                });
            }
            catch (ExpectedException ex)
            {
                return Utilidades.BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return Utilidades.InternalServerError(ex.Message);
            }
        }

        [Authorize("Bearer")]
        [HttpPut]
        [Route("editarProduto")]
        public IActionResult EditarProduto([FromServices]ProdutoAppServico produtoServico, [FromBody]ProdutoModelo produto)
        {
            try
            {
                produtoServico.EditarProduto(produto);

                return Ok(new
                {
                    Mensagem = "Produto alterado com sucesso."
                });
            }
            catch (ExpectedException ex)
            {
                return Utilidades.BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return Utilidades.InternalServerError(ex.Message);
            }            
        }

        [Authorize("Bearer")]
        [HttpDelete]
        [Route("excluirProduto/{id}/")]
        public IActionResult ExcluirProduto([FromServices]ProdutoAppServico produtoServico, int id)
        {
            try
            {
                produtoServico.ExcluirProduto(id);

                return Ok(new
                {
                    Mensagem = "Produto excluído com sucesso."
                });
            }
            catch (ExpectedException ex)
            {
                return Utilidades.BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return Utilidades.InternalServerError(ex.Message);
            }
        }

        [Authorize("Bearer")]
        [HttpGet]
        [Route("proverDetalheProduto/{id}/")]
        public IActionResult ProverDetalheProduto([FromServices]ProdutoAppServico produtoServico, int id)
        {
            try
            {
                ProdutoModelo produto = produtoServico.ProverDetalheProduto(id);

                return Ok(produto);
            }
            catch (ExpectedException ex)
            {
                return Utilidades.BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return Utilidades.InternalServerError(ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("proverDetalheTodosProdutos")]
        public async Task<IActionResult> Listar([FromServices]ProdutoAppServico produtoServico, int? numPagina)
        {
            try
            {
                IQueryable<Produto> produtos = produtoServico.ProverDetalheTodosProdutos();

                return View(await Paginacao<Produto>.CreateAsync(produtos.AsNoTracking(), numPagina ?? 1, 10));
            }
            catch (ExpectedException ex)
            {
                return Utilidades.BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return Utilidades.InternalServerError(ex.Message);
            }
        }

        private string ProverToken(UsuarioModelo usuario, TokenConfiguracoesModelo tokenConfiguracoes, ConfiguracoesAssinatura configuracoesAssinatura)
        {
            ClaimsIdentity identity = new ClaimsIdentity(new GenericIdentity(usuario.Id, "Login"));

            identity.AddClaim(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")));
            identity.AddClaim(new Claim(JwtRegisteredClaimNames.UniqueName, usuario.ChaveAcesso));

            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();

            SecurityTokenDescriptor descriptor = new SecurityTokenDescriptor();
            descriptor.Issuer = tokenConfiguracoes.Emissor;
            descriptor.Audience = tokenConfiguracoes.Audiencia;
            descriptor.SigningCredentials = configuracoesAssinatura.SigningCredentials;
            descriptor.Subject = identity;
            descriptor.NotBefore = DateTime.Now;
            descriptor.Expires = descriptor.NotBefore + TimeSpan.FromDays(1);

            SecurityToken securityToken = handler.CreateToken(descriptor);

            return handler.WriteToken(securityToken);            
        }
    }       
}
