﻿using Aplicacao.Modelos;
using AutoMapper;
using Dominio.Entidades;
using System.Linq;

namespace Aplicacao.MapeamentoAutomatico
{
    public class PerfilModeloMapeamento : Profile
    {
        public PerfilModeloMapeamento() : base("ViewToDomainModelMappings")
        {
            CreateMap<ProdutoModelo, Produto>();
            CreateMap<UsuarioModelo, Usuario>();
        }
    }
}
