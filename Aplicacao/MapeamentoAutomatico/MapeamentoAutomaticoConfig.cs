﻿using AutoMapper;

namespace Aplicacao.MapeamentoAutomatico
{
    public class MapeamentoAutomaticoConfig : Profile
    {
        public static void RegistrarMapeamento()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<DominioVisaoPerfil>();
                x.AddProfile<DominioVisaoPerfil>();
            });
        }
    }
}
