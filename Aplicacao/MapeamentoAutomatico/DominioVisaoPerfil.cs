﻿using Aplicacao.Modelos;
using AutoMapper;
using Dominio.Entidades;
using System.Linq;

namespace Aplicacao.MapeamentoAutomatico
{
    public class DominioVisaoPerfil : Profile
    {
        public DominioVisaoPerfil() : base("DomainToViewModelMappings")
        {
            CreateMap<Produto, ProdutoModelo>();
            CreateMap<Usuario, UsuarioModelo>();
        }
    }
}
