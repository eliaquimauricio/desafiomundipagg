﻿namespace Aplicacao.Modelos
{
    public class TokenConfiguracoesModelo
    {
        public string Audiencia { get; set; }

        public string Emissor { get; set; }
        
        public TokenConfiguracoesModelo()
        {
            Audiencia = "AudienciaPadrao";
            Emissor = "EmissorPadrao";
        }

    }
}
