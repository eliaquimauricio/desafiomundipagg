﻿namespace Aplicacao.Modelos
{
    public class ProdutoModelo
    {
        public int? Id { get; set; }

        public string Nome { get; set; }

        public double Valor { get; set; }

        public string Cor { get; set; }

        public double Peso { get; set; }

        public string Observacao { get; set; }
        
        public int PageIndex { get; set; }
               

        public ProdutoModelo()
        {
            Nome = string.Empty;
            Valor = -1;
            Cor = string.Empty;
            Peso = -1;
            Observacao = string.Empty;
        }
    }
}
