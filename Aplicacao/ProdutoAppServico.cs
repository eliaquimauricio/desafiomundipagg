﻿using Aplicacao.Modelos;
using AutoMapper;
using Dominio.Entidades;
using Dominio.Interfaces;
using System.Linq;

namespace Aplicacao
{
    public class ProdutoAppServico
    {
        private readonly IProdutoServico _produtoServico;

        public ProdutoAppServico(IProdutoServico produtoServico)
        {
            _produtoServico = produtoServico;
        }

        public int InserirProduto(ProdutoModelo produto)
        {
            return _produtoServico.InserirProduto(Mapper.Map<ProdutoModelo, Produto>(produto));
        }

        public void EditarProduto(ProdutoModelo produto)
        {
            _produtoServico.EditarProduto(Mapper.Map<ProdutoModelo, Produto>(produto));
        }

        public void ExcluirProduto(int id)
        {
            _produtoServico.ExcluirProduto(id);
        }

        public ProdutoModelo ProverDetalheProduto(int id)
        {
            return Mapper.Map<Produto, ProdutoModelo>(_produtoServico.ProverDetalheProduto(id));
        }

        public IQueryable<Produto> ProverDetalheTodosProdutos()
        {
            return _produtoServico.ProverDetalheTodosProdutos();            
        }
    }
}
