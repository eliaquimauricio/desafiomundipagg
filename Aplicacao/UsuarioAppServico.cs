﻿using Aplicacao.Modelos;
using AutoMapper;
using Dominio.Entidades;
using Dominio.Interfaces;
using Dominio.Servicos;

namespace Aplicacao
{
    public class UsuarioAppServico
    {
        private readonly IUsuarioServico _usuarioServico;

        public UsuarioAppServico(IUsuarioServico usuarioServico)
        {
            _usuarioServico = usuarioServico;
        }

        public void LogarUsuario(UsuarioModelo usuario)
        {
            _usuarioServico.LogarUsuario(Mapper.Map<UsuarioModelo, Usuario>(usuario));
        }
    }
}
