﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Aplicacao.Utilidades
{
    public static class Utilidades
    {
        public static string ObterHeader(HttpRequest request, string nome)
        {
            return request.Headers.ContainsKey(nome) ? request.Headers[nome].ToString() : string.Empty;
        }

        public static ObjectResult InternalServerError(string mensagemErro)
        {
            return ErroGenerico(HttpStatusCode.InternalServerError, mensagemErro);
        }
        
        public static ObjectResult BadRequest(string mensagemErro)
        {
            return ErroGenerico(HttpStatusCode.BadRequest, mensagemErro);
        }
        
        private static ObjectResult ErroGenerico(HttpStatusCode codigoStatus, string mensagemErro)
        {
            return new ObjectResult(new { Error = mensagemErro }) { StatusCode = (int)codigoStatus };
        }
    }
}
